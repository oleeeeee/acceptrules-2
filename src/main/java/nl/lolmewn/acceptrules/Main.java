package nl.lolmewn.acceptrules;

import nl.lolmewn.acceptrules.Updater.UpdateType;
import nl.lolmewn.acceptrules.commands.ARCommand;
import nl.lolmewn.acceptrules.commands.RulesCommand;
import org.bstats.bukkit.Metrics;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends JavaPlugin {

    private final PlayerManager playerManager = new PlayerManager();
    private final RulesManager rulesManager = new RulesManager();
    private final LinkedHashMap<String, List<String>> readRules = new LinkedHashMap<String, List<String>>();
    private File userFile;
    private File rulesFile;
    private File newRulesFile;

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
        this.checkFiles();
        this.loadUsers();
        this.loadRules();
        final Main plugin = this;
        this.getServer().getScheduler().runTask(this, () -> {
            getServer().getPluginCommand("acceptrules").setExecutor(new ARCommand(plugin));
            try {
                getServer().getPluginCommand("rules").setExecutor(new RulesCommand(plugin));
            } catch (NullPointerException e) {
                getLogger().warning("Failed to register the /rules command - using hacky and sketchy work-around.");
                new RulesCmdHack(plugin);
            }
        });
        this.getServer().getPluginManager().registerEvents(new Events(this), this);
        if (this.getConfig().getBoolean("update", true)) {
            new Updater(this, 31707, this.getFile(), UpdateType.DEFAULT, true);
        }
        new Metrics(this);
    }

    public PlayerManager getAcceptedPlayers() {
        return playerManager;
    }

    public RulesManager getRulesManager() {
        return rulesManager;
    }

    public LinkedHashMap<String, List<String>> getUsersWhoReadRules() {
        return readRules;
    }

    private void checkFiles() {
        this.userFile = new File(this.getDataFolder(), "users.dat");
        File userFileOrig = new File(this.getDataFolder(), "users_converted.dat");
        this.rulesFile = new File(this.getDataFolder(), "rules.txt");
        this.newRulesFile = new File(this.getDataFolder(), "rules.yml");
        if (!userFile.exists()) {
            userFile.getParentFile().mkdirs();
            try {
                userFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (!userFileOrig.exists() && !this.getConfig().getBoolean("converted", false)) {
            this.loadUsers();
            userFile.renameTo(userFileOrig);
            userFile = new File(this.getDataFolder(), "users.dat"); //Don't know to which one it would point otherwise
            try {
                userFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            HashSet<String> uuids = new HashSet<String>();
            for (String user : this.playerManager) {
                uuids.add(this.getServer().getOfflinePlayer(user).getUniqueId().toString());
            }
            this.playerManager.clear();
            this.playerManager.addAll(uuids);
            this.getConfig().set("converted", true);
            this.saveConfig();
            this.saveUsers();
        }
        if (!this.rulesFile.exists() && !this.newRulesFile.exists()) {
            this.saveResource("rules.yml", true);
        } else if (!this.newRulesFile.exists()) {
            try {
                this.newRulesFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (rulesFile.exists()) {
                convertOldRulesFile();
            }
        }
    }

    public void loadUsers() {
        if (!userFile.exists()) {
            this.checkFiles();
        }
        try {
            BufferedReader reader = new BufferedReader(new FileReader(userFile));
            String player;
            while ((player = reader.readLine()) != null) {
                if (player.contains(";")) {
                    //old format
                    String[] split = player.split(";");
                    this.playerManager.addAll(Arrays.asList(split));
                    saveUsers();
                    return;
                }
                this.playerManager.add(player);
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void oldLoadRules() {
        if (!rulesFile.exists()) {
            this.checkFiles();
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(rulesFile))) {
            String rule;
            ArrayList<String> ruleList = new ArrayList<>();
            while ((rule = reader.readLine()) != null) {
                if (ruleList.size() == 8) {
                    this.rulesManager.put(Integer.toString(this.rulesManager.size()), ruleList);
                    ruleList = new ArrayList<>();
                }
                ruleList.add(rule);
            }
            this.rulesManager.put(Integer.toString(this.rulesManager.size()), ruleList);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveUsers() {
        try (BufferedWriter output = new BufferedWriter(new FileWriter(this.userFile, false))) {
            for (String user : this.playerManager) {
                output.append(user);
                output.newLine();
                output.flush();
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveUser(String name) {
        try (BufferedWriter output = new BufferedWriter(new FileWriter(this.userFile, true))) {
            output.append(name);
            output.newLine();
            output.flush();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void convertOldRulesFile() {
        this.oldLoadRules();
        YamlConfiguration rulesYml = YamlConfiguration.loadConfiguration(this.newRulesFile);
        if (this.getConfig().getBoolean("usePagination", true)) {
            for (int i = 0; i < this.rulesManager.size(); i++) {
                List<String> rules = this.rulesManager.get(Integer.toString(i));
                rulesYml.set("" + (i + 1), rules); //there's only one in there anyway
            }
        } else {
            rulesYml.set("1", this.rulesManager);
        }
        try {
            rulesYml.save(this.newRulesFile);
            this.rulesFile.delete();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadRules() {
        YamlConfiguration c = YamlConfiguration.loadConfiguration(this.newRulesFile);
        for (String keyset : c.getConfigurationSection("").getKeys(false)) {
            this.rulesManager.put(keyset, c.getStringList(keyset));
        }
    }
}
